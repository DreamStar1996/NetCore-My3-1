﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Core.Api.Controllers
{
    /// <summary>
    /// Test Core,基类
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseController : Controller
    {
    }
}
