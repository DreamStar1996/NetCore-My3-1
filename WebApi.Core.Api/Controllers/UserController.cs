﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Core.Model;

namespace WebApi.Core.Api.Controllers
{
    public class UserController : BaseController
    {
        /// <summary>
        /// 测试 Hello
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        //[ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult Hello()
        {
            return Ok("Hello World!");
        }

        /// <summary>
        /// 用户实体类测试
        /// </summary>
        /// <param name="usersModel"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UsersTestSwagger(UsersModel usersModel)
        {
            return Ok(usersModel);
        }
    }
}
