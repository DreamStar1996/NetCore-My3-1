﻿using System;

namespace WebApi.Core.Model
{
    /// <summary>
    /// 用户实体类
    /// </summary>
    public class UsersModel
    {
        ///<summary>
        /// GID
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }
    }
}
